 <!-- ======= Header ======= -->
 <section id="topbar" class="topbar bg-dark d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@example.com">info@bmkg.go.id</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4"><span>196</span></i>
      </div>
      <div class="social-links d-none d-md-flex align-items-center">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </section><!-- End Top Bar -->

  <header id="header" class="header d-flex align-items-center bg-white  ">

    <div class="container-fluid container-xl d-flex align-items-center justify-content-between " >
      <a href="{{url('/')}}" class="logo d-flex align-items-center ">
        <!-- Uncomment the line below if you also wish to use an image logo -->
         
            <img  src="{{url('img/logo-bmkg.png')}}" alt="Logo BMKG"> 
         
        
         <h6 class="text-dark m-2">BMKG<span></span></h6>
        
      </a>
      <nav id="navbar" class="navbar ">
        <ul>
          <li ><a href="{{url('/')}}">Beranda</a></li>
          <li ><a href="#">Cuaca</a></li>
          <li><a href="#">Gempabumi & Tsunami</a></li>
          <li><a href="#">Iklim</a></li>
          <li><a href="#">Berita</a></li>
          <li><a href="#">Profil</a></li>
         
         
         
        </ul>
      </nav><!-- .navbar -->

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
  </header><!-- End Header -->
  <!-- End Header -->